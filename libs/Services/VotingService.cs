using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace module
{
    public class VotingService : IVotingService
    {
        private ICourseService _courseService;
        private IHttpClientFactory _clientFactory;
        List<VotingModel> modelList;

        public VotingService(ICourseService courseService, IHttpClientFactory clientFactory)
        {
            _courseService = courseService;
            _clientFactory = clientFactory;
            modelList = new List<VotingModel>();
        }

        public async Task<List<VotingModel>> GetAllVotesAsync() // NOT DONE
        {
            // Logic for getting all votes
            try 
            {
                var client = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Get, "http://dataservice:80/vote");
                // DB is empty, come back later
                var responseMsg= await client.SendAsync(msgreq);
                var ServiceResponse = await responseMsg.Content.ReadAsAsync<List<VotingModel>>();
                modelList.AddRange(ServiceResponse);
                System.Console.WriteLine("--------------------");
                foreach(VotingModel vm in modelList) {
                    System.Console.WriteLine(vm.Id);
                }
                System.Console.WriteLine("--------------------");

            } catch (System.Exception e) 
            {
                System.Console.WriteLine(e);
            }
            return modelList;
        }
        public async Task<VotingModel> GetVoteAsync(string id) // NOT DONE
        {
            VotingModel res = new VotingModel();
            try
            {
                var client = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Get, "http://dataservice:80/vote/"+id);
                var responseMsg= await client.SendAsync(msgreq);
                var ServiceResponse = await responseMsg.Content.ReadAsAsync<VotingModel>();
                res = ServiceResponse;

            } catch (System.Exception e)
            {
                System.Console.WriteLine(e);
            }
            return res;
        }
        public async Task<string> CreateNewVote(string[] course_ids) // NOT DONE
        {
            string res = null;
            try
            {
                CourseIdModel idModel = new CourseIdModel() {
                    CourseIds = course_ids.ToList()
                };
                var client = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Post, "http://dataservice:80/vote");
                msgreq.Content = new StringContent(JsonConvert.SerializeObject(idModel),Encoding.UTF8, "application/json");
                var responseMsg= await client.SendAsync(msgreq);
                var content = await responseMsg.Content.ReadAsAsync<dynamic>();
                res = content["Id"];
            } catch (System.Exception e)
            {
                System.Console.WriteLine(e);
            }
            return res;
        }
        public async Task<bool> AddVotesToVote(string[] course_ids) // READY FOR TESTING
        {
            var res = false;
            try 
            {
                CourseIdModel idModel = new CourseIdModel() {
                    CourseIds = course_ids.ToList()
                };
                var cliet = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Post, "http://dataservice:80/vote/addVotes");
                msgreq.Content = new StringContent(JsonConvert.SerializeObject(idModel),Encoding.UTF8, "application/json");
                await cliet.SendAsync(msgreq);
                res = true;
            } catch (System.Exception e)
            {
                System.Console.WriteLine(e);
                res = false;
            }
            return res;
        }
        public async Task<bool> DisableVote(string id) // NOT DONE
        {
            var res = false;
            try 
            {
                var cliet = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Patch, "http://dataservice:80/vote/" + id + "/disable");
                var responseMsg= await cliet.SendAsync(msgreq);
                await cliet.SendAsync(msgreq);
                res = true;
            } catch (System.Exception e)
            {
                System.Console.WriteLine(e);
                res = false;
            }
            return res;
        }

      
    }
}