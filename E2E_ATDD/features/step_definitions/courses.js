const chai = require('chai');
var expect = chai.expect;
const { Given, When, Then, BeforeAll, Before, AfterAll } = require('cucumber');
const {Builder, By, Key, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const util = require('util')
const fs = require('fs')
const writeFile = util.promisify(fs.writeFile)

const root = process.env.url || "http://127.0.0.1:8080"


async function getDriver() {
    return new Builder().forBrowser('chrome')
        .setChromeOptions(new chrome.Options().headless().windowSize({
                width: 1000,
                height: 1000
            })
                .addArguments("--no-sandbox")
                .addArguments("--disable-dev-shm-usage")
        )
        .build();
}


BeforeAll(async function (){
    driver = await getDriver();
})

AfterAll(async function (){
    try {
        await driver.quit();
    } catch (err) { 
    }
})

Given('User is logged in', async function () {
    driver.manage().deleteAllCookies();
    await driver.get(root+"/login/login"); 

    await driver.findElement(By.id("UserName")).sendKeys("test");
    await driver.findElement(By.id("Password")).sendKeys("test");
           
    await driver.findElement(By.id('submit_id')).click();
    await writeFile("/artifact/" + Date.now()+"_atdd_User is logged in.png", await driver.takeScreenshot(), 'base64')

});

Given('User is on new Createnew course page', async function () {
    await driver.get(root+"/Course/CreateCourse")
    await writeFile("/artifact/" + Date.now()+"_atdd_User is on new Createnew course page.png", await driver.takeScreenshot(), 'base64')
});

Given('User is on edit course page', async function () {
    await driver.get(root+"/Course/AllCourses")
    let firstElement = (await driver.findElements(By.css(".course-container")))[0]
    await firstElement.findElement(By.id("edit_id")).click()
    await writeFile("/artifact/" + Date.now()+"_atdd_User is on edit course page.png", await driver.takeScreenshot(), 'base64')
});

Given('User is on show All courses page', async function () {
    await driver.get(root+"/Course/AllCourses")
    await writeFile("/artifact/" + Date.now()+"_atdd_User is on show All courses page.png", await driver.takeScreenshot(), 'base64')
});


Given('User is on Managing courses page', async function () {
    await driver.get(root+"/Course") 
    await writeFile("/artifact/" + Date.now()+"_atdd_User is on Managing courses page.png", await driver.takeScreenshot(), 'base64')
});

Given('User has enterd appropriate course data', async function () {
    await driver.findElement(By.id("CourseName")).sendKeys("SomeCourseName");
    await driver.findElement(By.id("Room")).sendKeys("RoomCode123");
    await writeFile("/artifact/" + Date.now()+"_atdd_User has enterd appropriate course data.png", await driver.takeScreenshot(), 'base64')
});

let nextCourseId = 0;
Given('User has enterd appropriate course data using existing teacherId and identical semester', async function () {
    await driver.findElement(By.id("CourseName")).sendKeys("Dart 10"+(++nextCourseId));
    await driver.findElement(By.id("TeacherId")).sendKeys("34");
    await driver.findElement(By.id("Semester")).sendKeys("2019_Fall");    
    await writeFile("/artifact/" + Date.now()+"_atdd_User has enterd appropriate course data using existing teacherId and identical semester.png", await driver.takeScreenshot(), 'base64')
});

Given('User has enterd desired modification', async function () {
    await driver.findElement(By.id("CourseName")).sendKeys("SomeCourseName");
    await driver.findElement(By.id("Room")).sendKeys("RoomCode007");
    await writeFile("/artifact/" + Date.now()+"_atdd_User has enterd desired modification.png", await driver.takeScreenshot(), 'base64')
});

Given('User has enterd appropriate max number of students', async function () {
    await driver.findElement(By.id("StudentNumberModel_maximumstudents")).sendKeys("7");
    await writeFile("/artifact/" + Date.now()+"_atdd_User has enterd appropriate max number of students.png", await driver.takeScreenshot(), 'base64')
});

Given('User is on Template create new course page', async function () {
    await driver.get(root+"/Course/AllCourses")
    let firstElement = (await driver.findElements(By.css(".course-container")))[0]
    await firstElement.findElement(By.id("edit_id")).click()
    await writeFile("/artifact/" + Date.now()+"_atdd_User is on Template create new course page.png", await driver.takeScreenshot(), 'base64')
});


When('User clicks submit', async function () {
    await driver.findElement(By.css("[type=submit]")).click();
    await writeFile("/artifact/" + Date.now()+"_atdd_User clicks submit.png", await driver.takeScreenshot(), 'base64')
});

When('User clicks Show all courses button', async function () {
    await driver.findElement(By.linkText("SHOW ALL COURSES")).click();
    await writeFile("/artifact/" + Date.now()+"_atdd_User clicks Show all courses button.png", await driver.takeScreenshot(), 'base64')
});

When('User clicks Delete button', async function () {
    await driver.findElement(By.id("delete_id")).click();
    await writeFile("/artifact/" + Date.now()+"_atdd_User clicks Delete button.png", await driver.takeScreenshot(), 'base64')
});

When('User click Bulk delete courses button', async function () {
    await driver.findElement(By.linkText("BULK DELETE COURSES")).click();
    await writeFile("/artifact/" + Date.now()+"_atdd_User click Bulk delete courses button.png", await driver.takeScreenshot(), 'base64')
});

When('User clicks Requery button', async function () {
    await driver.findElement(By.css("[value=Requery]")).click();
    await writeFile("/artifact/" + Date.now()+"_atdd_User clicks Requery button.png", await driver.takeScreenshot(), 'base64')
});

Then('User should see appropriate user-message', async function () {
    await writeFile("/artifact/" + Date.now()+"_atdd_User should see appropriate user-message.png", await driver.takeScreenshot(), 'base64')
    expect(await driver.findElement(By.id("userMsg_id")).getText()).contains("Success");
});

Then('User should see appropriate error-message regarding teacherId', async function () {
    await writeFile("/artifact/" + Date.now()+"_atdd_User should see appropriate error-message regarding teacherId.png", await driver.takeScreenshot(), 'base64')
    expect(await driver.findElement(By.id("ErrMsg_id")).getText()).contains("Can not assign teacher to course");
});

Then('User should see list of appropriate courses', async function () {
    let courses = await driver.findElements(By.css(".course-container"))
    await writeFile("/artifact/" + Date.now()+"_atdd_User should see list of appropriate courses.png", await driver.takeScreenshot(), 'base64')
    expect(courses.length).to.be.at.least(2)
});

