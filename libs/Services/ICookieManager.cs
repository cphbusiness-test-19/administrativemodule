using Microsoft.AspNetCore.Http;

namespace module
{
    public interface ICookieManager
    {
        void SetOptions(bool IsEssential, bool HttpOnly);
        CookieOptions GetOptions();
        void Append(string Name, string Data, CookieOptions opts, HttpResponse response);
        void Delete(string Name, HttpResponse response);   
    }
}