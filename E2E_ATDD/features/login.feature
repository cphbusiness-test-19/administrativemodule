Feature: Login feature

    Scenario: login into the system (success)
        Given User is not logged into system
        Given User is on loginscreen
        When User enters correct credentials
        Then User should be allowed access to landingpage

    Scenario: login into the system (failure)
        Given User is not logged into system
        Given User is on loginscreen
        When User enters incorrect credentials
        Then User is shown appropriate errormessage

    Scenario: login into the system (failure 3 times)
        Given User is not logged into system
        Given User is on loginscreen
        When User enters incorrect credentials
        When User enters incorrect credentials
        When User enters incorrect credentials
        When User enters correct credentials
        Then User is shown appropriate errormessage
