/*

https://stackoverflow.com/questions/22426273/chromeoptions-causes-reference-error-using-selenium-chromedriver-for-node-js
PATH="$PATH:/Developer/Tools"

*/


const chai = require('chai');
var expect = chai.expect;
const { Given, When, Then, BeforeAll, Before, AfterAll } = require('cucumber');
const {Builder, By, Key, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const util = require('util')
const fs = require('fs')
const writeFile = util.promisify(fs.writeFile)

const root = process.env.url || "http://127.0.0.1:8080"


function getDriver() {
    return new Builder().forBrowser('chrome')
        .setChromeOptions(new chrome.Options().headless().windowSize({
                width: 1000,
                height: 1000
            })
                .addArguments("--no-sandbox")
                .addArguments("--disable-dev-shm-usage")
        )
        .build();
}


BeforeAll(async function (){
    driver = await getDriver();
})


AfterAll(async function (){
    try {
        await driver.quit();
    } catch (err) { 
    }
})



Given('User is not logged into system', async function () {
    driver.manage().deleteAllCookies();
});

Given('User is on loginscreen', async function () {
    await driver.get(root+"/login/login"); 
    await writeFile("/artifact/" + Date.now()+"_atdd_User is on loginscreen.png", await driver.takeScreenshot(), 'base64')
});

When('User enters correct credentials', async function () {
    await driver.findElement(By.id("UserName")).sendKeys("test");
    await driver.findElement(By.id("Password")).sendKeys("test");
    await writeFile("/artifact/" + Date.now()+"_atdd_User enters correct credentials.png", await driver.takeScreenshot(), 'base64')
           
    await driver.findElement(By.id('submit_id')).click();
});

When('User enters incorrect credentials', async function () {
    await driver.findElement(By.id("UserName")).sendKeys("no user");
    await driver.findElement(By.id("Password")).sendKeys("with wrong password");
    await writeFile("/artifact/" + Date.now()+"_atdd_User enters incorrect credentials.png", await driver.takeScreenshot(), 'base64')
               
    await driver.findElement(By.id('submit_id')).click();
});

Then('User should be allowed access to landingpage', async function () {
    await writeFile("/artifact/" + Date.now()+"_atdd_User should be allowed access to landingpage.png", await driver.takeScreenshot(), 'base64')
    expect(await driver.getTitle()).to.contain("Landing");
});

Then('User is shown appropriate errormessage', async function () {
    expect(await driver.getTitle()).to.contain("Login");
    expect(await driver.findElement(By.id("message_id")).getText()).contains("Credentials incorrect")
    await writeFile("/artifact/" + Date.now()+"_atdd_User is shown appropriate errormessage.png", await driver.takeScreenshot(), 'base64')
});
