using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace module.Controllers
{
    public class VotingController : Controller
    {
        private IVotingService _voteService;
        private ICourseService _courseService;
        
        public VotingController(IVotingService voteService, ICourseService courseService)
        {
            _voteService = voteService;
            _courseService = courseService;
        }
        
        public IActionResult Index() 
        {        
            ViewBag.userMsg=TempData["cc"];
            return View();
        }

        [HttpGet] // Get Vote
        public async Task<IActionResult> Vote(string id) 
        {
            var model = await _voteService.GetVoteAsync(id);

            return View(model);
        }
        
        [HttpPost, ActionName("DisableVote")]// Disable Vote
        public async Task<IActionResult> DisableVote(string VoteId) 
        {
            var res = await _voteService.DisableVote(VoteId);

            return RedirectToAction("Index");
        }
        
        [HttpPost] // Add votes to Vote
        public async Task<IActionResult> Vote(string[] courseCheckbox) 
        {
            var res = await _voteService.AddVotesToVote(courseCheckbox);
            
            return RedirectToAction("Index");
        }
        
        [HttpGet] // Get CreateVote page
        public async Task<IActionResult> CreateVote() {
            List<CourseModel> _courses = await _courseService.GetAllCoursesAsync(0);
            System.Console.WriteLine("----------------CreateVote----------------");
            foreach(CourseModel cm in _courses) {
                System.Console.WriteLine(cm.CourseName);
            }
            System.Console.WriteLine("------------------------------------------");
            VotingViewModel model = new VotingViewModel() {
                AllCourses = _courses
            };
            return View(model);
        }

        [HttpPost] // Create new Vote
        public async Task<IActionResult> CreateVote(string[] courseCheckbox)
        {
            string newId = await _voteService.CreateNewVote(courseCheckbox);

            return RedirectToAction("AllVotes");
        }
        
        public async Task<IActionResult> AllVotes()
        {
            var model = await _voteService.GetAllVotesAsync();
            System.Console.WriteLine("----------Controller---------");
            foreach( VotingModel vm in model) {
                System.Console.WriteLine(vm.Id);
            }
            System.Console.WriteLine("-------------END------------");
            return View(model);
        }
    }
}