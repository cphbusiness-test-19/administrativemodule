Feature: CRUDCourses

    CRUD-operations for course-data

    Scenario: Create new course
        Given User is logged in
        Given User is on new Createnew course page
        Given User has enterd appropriate course data
        When User clicks submit
        Then User should see appropriate user-message

    Scenario: Create new course fails if teacherId already used 3 times for same semester
        Given User is logged in

        Given User is on new Createnew course page
        Given User has enterd appropriate course data using existing teacherId and identical semester
        When User clicks submit

        Given User is on new Createnew course page
        Given User has enterd appropriate course data using existing teacherId and identical semester
        When User clicks submit

        Given User is on new Createnew course page
        Given User has enterd appropriate course data using existing teacherId and identical semester
        When User clicks submit

        Given User is on new Createnew course page
        Given User has enterd appropriate course data using existing teacherId and identical semester
        When User clicks submit
        Then User should see appropriate error-message regarding teacherId

    Scenario: Read course
        Given User is logged in
        Given User is on Managing courses page
        When User clicks Show all courses button
        Then User should see list of appropriate courses

    Scenario: Update(edit) course
        Given User is logged in
        Given User is on edit course page
        Given User has enterd desired modification
        When User clicks submit
        Then User should see appropriate user-message

    Scenario: Delete course
        Given User is logged in
        Given User is on show All courses page
        When User clicks Delete button
        Then User should see appropriate user-message

    Scenario: Bulk delete courses older than 6 semesters (3 years)
        Given User is logged in
        Given User is on Managing courses page
        When User click Bulk delete courses button
        Then User should see appropriate user-message

    Scenario: Show courses with less than X students signed up
        Given User is logged in
        Given User is on show All courses page
        Given User has enterd appropriate max number of students 
        When User clicks Requery button
        Then User should see list of appropriate courses

    Scenario: Reuse course data as temple for new course
        Given User is logged in
        Given User is on Template create new course page
        When User clicks submit
        Then User should see appropriate user-message
    