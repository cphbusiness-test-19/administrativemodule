using System.Collections.Generic;
using System.Threading.Tasks;

namespace module
{
    public interface IVotingService
    {
        Task<List<VotingModel>> GetAllVotesAsync();
        Task<VotingModel> GetVoteAsync(string id);
        Task<string> CreateNewVote(string[] course_ids);
        Task<bool> AddVotesToVote(string[] course_ids);
        Task<bool> DisableVote(string id);
    }
}