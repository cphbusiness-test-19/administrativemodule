using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using module;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Tests
{
    [Category("unit-test")]
    [Category("fast")]
    public class CourseServiceTests
    {
        CourseService courseService;
        CourseModel courseData;

        public void helper_SetupCourse(){
            courseData.Archived=false;
            courseData.Category="Music";
            courseData.CourseName="Jazzy Tuba";
            courseData.Description="some text here";
            courseData.Id="someOBJECTIDhere";
            courseData.Language="Danish";
            courseData.Level="Beginner";
            courseData.NoHoursPrWeek=4;
            courseData.Room="soundproof-01A";
            courseData.Semester="2019_fall";
            courseData.Students=new List<int>(){1,2,3,4,5};
            courseData.TeacherId=23;
            courseData.Visible=true;
            courseData.Weekdays="Thursday";
        }


        [SetUp]
        public void setup(){
            courseData = new CourseModel();
        }



        [Test]
        public void smalltest(){
            Assert.Pass();
        }
        
        [Test]
        public async Task CreateCourse_valid_microserviceCall_returnsTrue(){

            var JsonResponse = "";  

            var fakeHttpMessageHandler = new MockHttpClientResponse(new HttpResponseMessage() 
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonResponse, Encoding.UTF8, "application/json") 
            });

            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);
            var mockFactory = new Mock<IHttpClientFactory>();
            mockFactory.Setup(x=>x.CreateClient(It.IsAny<string>())).Returns(fakeHttpClient);
            
            var factory = mockFactory.Object;
            
            courseService = new CourseService(factory);

            var actual = await courseService.CreateCourse(courseData);
            
            Assert.NotNull(courseService);
            Assert.True(actual);
        }

        [Test]
        public void AdjustVals_nullDataChangesToEmptyString_returnsUpdatedCourseModel(){
            courseData.Room=null;
            courseData.Students=null;
            courseData.Description=null;
            courseData.Semester=null;
            
            var mockFactory = new Mock<IHttpClientFactory>();
            var factory = mockFactory.Object;
            
            courseService = new CourseService(factory);
            var actual = courseService.AdjustVals(courseData);

            Assert.That(actual.Room, Is.EqualTo(String.Empty));
            Assert.That(actual.Students, Is.EqualTo(new List<int>()));
            Assert.That(actual.Description, Is.EqualTo(String.Empty));
            Assert.That(actual.Semester, Is.EqualTo(String.Empty));
        }


        [Test]
        public async Task DeleteCourse_valid_microserviceCall_returnsTrue(){
            string id = "42";
            var JsonResponse = "";

            var fakeHttpMessageHandler = new MockHttpClientResponse(new HttpResponseMessage() 
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonResponse, Encoding.UTF8, "application/json") 
            });

            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);
            var mockFactory = new Mock<IHttpClientFactory>();
            mockFactory.Setup(x=>x.CreateClient(It.IsAny<string>())).Returns(fakeHttpClient);
            
            var factory = mockFactory.Object;
            
            courseService = new CourseService(factory);

            var actual = await courseService.DeleteCourse(id);
            
            Assert.NotNull(courseService);
            Assert.True(actual);
        }

        [Test]
        public async Task getAllCourses_DefaultStudentValue_valid_microserviceCall_returnsListofCourses(){
            List<CourseModel> respList = new List<CourseModel>(){
                new CourseModel(),
                new CourseModel()
            };
            var number = -1; // indicates all students
            var JsonResponse = JsonConvert.SerializeObject(respList); 

            var fakeHttpMessageHandler = new MockHttpClientResponse(new HttpResponseMessage() 
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonResponse, Encoding.UTF8, "application/json") 
            });

            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);
            var mockFactory = new Mock<IHttpClientFactory>();
            mockFactory.Setup(x=>x.CreateClient(It.IsAny<string>())).Returns(fakeHttpClient);
            
            var factory = mockFactory.Object;
            
            courseService = new CourseService(factory);

            var actual = await courseService.GetAllCoursesAsync(number);
            
            Assert.NotNull(courseService);
            Assert.That(actual.Count, Is.EqualTo(2));
        }

        //this one is a little meh, all the logic is moved inside microservice
        [Test]
        public async Task getAllCourses_lessThen6Students_valid_microserviceCall_returnsListofCourses(){
            List<CourseModel> respList = new List<CourseModel>(){
                new CourseModel(){}
            };
            var number = 6; // indicates all students
            var JsonResponse = JsonConvert.SerializeObject(respList); 

            var fakeHttpMessageHandler = new MockHttpClientResponse(new HttpResponseMessage() 
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonResponse, Encoding.UTF8, "application/json") 
            });

            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);
            var mockFactory = new Mock<IHttpClientFactory>();
            mockFactory.Setup(x=>x.CreateClient(It.IsAny<string>())).Returns(fakeHttpClient);
            
            var factory = mockFactory.Object;
            
            courseService = new CourseService(factory);

            var actual = await courseService.GetAllCoursesAsync(number);
            
            Assert.NotNull(courseService);
            Assert.That(actual.Count, Is.EqualTo(1));
        }


        [Test]
        public async Task getSingleCourse_valid_microserviceCall_returnsCourse(){
            string id = "42";
            var JsonResponse = JsonConvert.SerializeObject(courseData);

            var fakeHttpMessageHandler = new MockHttpClientResponse(new HttpResponseMessage() 
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonResponse, Encoding.UTF8, "application/json") 
            });

            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);
            var mockFactory = new Mock<IHttpClientFactory>();
            mockFactory.Setup(x=>x.CreateClient(It.IsAny<string>())).Returns(fakeHttpClient);
            
            var factory = mockFactory.Object;
            
            courseService = new CourseService(factory);

            var actual = await courseService.GetCourseAsync(id);
            
            Assert.NotNull(courseService);
            Assert.NotNull(actual);
        }

        [Test]
        public async Task UpdateCourse_valid_microserviceCall_returnsTrue(){
            var JsonResponse = "";  

            var fakeHttpMessageHandler = new MockHttpClientResponse(new HttpResponseMessage() 
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonResponse, Encoding.UTF8, "application/json") 
            });

            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);
            var mockFactory = new Mock<IHttpClientFactory>();
            mockFactory.Setup(x=>x.CreateClient(It.IsAny<string>())).Returns(fakeHttpClient);
            
            var factory = mockFactory.Object;
            
            courseService = new CourseService(factory);

            var actual = await courseService.UpdateCourse(courseData);
            
            Assert.NotNull(courseService);
            Assert.True(actual);
        }
        

        [Test]
        public void ExtractData_leaveIdandVotesBehind_returnssmallCourseModel(){
            helper_SetupCourse();
            var mockFactory = new Mock<IHttpClientFactory>(); //no setup required 
            var factory = mockFactory.Object;
            courseService = new CourseService(factory);
            //setting data to copy
            var actual = courseService.ExtractSenderData(courseData);

            PropertyInfo[] distArr = typeof(smallCourseModel).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            PropertyInfo[] srcArr = typeof(CourseModel).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var item in distArr)
            {
                //find the name of the field
                var srcName = srcArr.FirstOrDefault(x=>x.Name == item.Name);
                Assert.That(item.GetValue(actual), Is.EqualTo(srcName.GetValue(courseData)));
            }
        }


        [Test]
        public void ExtractData_leaveVotesBehind_returnsUpdateCourseModel(){
            helper_SetupCourse();            
            var mockFactory = new Mock<IHttpClientFactory>(); //no setup required 
            var factory = mockFactory.Object;
            courseService = new CourseService(factory);
            //setting data to copy

            var actual = courseService.ExtractSenderDataId(courseData);

            PropertyInfo[] distArr = typeof(UpdateCourseModel).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            PropertyInfo[] srcArr = typeof(CourseModel).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var item in distArr)
            {
                //find the name of the field
                var srcName = srcArr.FirstOrDefault(x=>x.Name == item.Name);
                Assert.That(item.GetValue(actual), Is.EqualTo(srcName.GetValue(courseData)));
            }
        }
    }
}