FROM mcr.microsoft.com/dotnet/core/sdk as base
WORKDIR /app
EXPOSE 80

COPY administrativemodule.sln ./
COPY libs/*.csproj ./libs/
COPY module/*.csproj ./module/
COPY tests/*.csproj ./tests/

RUN dotnet restore
COPY . .
RUN dotnet publish -c Release -o /app
CMD ["dotnet", "module.dll"]




