using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace module
{
    public class AuthService : IAuthService
    {
        private IHttpClientFactory _clientFactory;
        public AuthService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }
        public async Task<UserModel> VerifyCredentials(UserModel userData){

            SmallUsermodel requestModel = new SmallUsermodel();
            requestModel.username=userData.UserName;
            requestModel.password=userData.Password;
            requestModel.grant_type="password";

            try
            {
                var client = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Post, "http://authservice:32001/token");
                msgreq.Content = new StringContent(JsonConvert.SerializeObject(requestModel),Encoding.UTF8, "application/json");

                var responseMsg= await client.SendAsync(msgreq);
                var ServiceResponse = await responseMsg.Content.ReadAsAsync<dynamic>();

                if(ServiceResponse["error"]!=null){                
                    userData=null;
                }else{
                    userData.Token = ServiceResponse["access_token"];
                }
            }
            catch (System.Exception)
            {
                //failed to connect to microservice -> results in failed login, userdata is reset
                System.Console.WriteLine("Warning - unable to connection to authService login");
                userData=null;
            }
            return userData;
        }
    }
}