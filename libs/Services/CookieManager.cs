using Microsoft.AspNetCore.Http;

namespace module
{
    public class CookieManager : ICookieManager
    {
        private CookieOptions Coptions;
        public CookieManager()
        {
            Coptions = new CookieOptions();
        }
        public void Append(string Name, string Data, CookieOptions opts, HttpResponse response)
        {
            response.Cookies.Append(Name,Data,GetOptions());
        }

        public void Delete(string Name, HttpResponse response)
        {
            response.Cookies.Delete(Name);
        }

        public CookieOptions GetOptions()
        {
            return Coptions;
        }

        public void SetOptions(bool _isEssential, bool _httpOnly)
        {
            Coptions.HttpOnly = _httpOnly;
            Coptions.IsEssential = _isEssential;
        }
    }
}