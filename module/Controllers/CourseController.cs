using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace module
{
    [Authorize]
    public class CourseController : Controller
    {
        private ICourseService _courseService;
        public CourseController(ICourseService courseService)
        {
            _courseService = courseService;
        }

        public IActionResult Index(){
            
            ViewBag.userMsg=TempData["cc"];
            return View();
        }

        public bool teacherHasToManyCourses(int teacherId, List<CourseModel> courses, string semester){
            var counter = courses.Where(x => x.TeacherId == teacherId && x.Semester == semester).ToList().Count;
            if (counter >= 3 )
                return true;
            
            return false;
        }


        //display
        [HttpGet]
        public IActionResult CreateCourse(){
            return View();
        }

        //receive -> then post to api
        [HttpPost]
        public async Task<IActionResult> CreateCourse(CourseModel courseData){
            if(!ModelState.IsValid){
                return View(courseData);
            }

            var courses = await _courseService.GetAllCoursesAsync(0);

            if (teacherHasToManyCourses(courseData.TeacherId, courses, courseData.Semester)){
                ViewBag.ErrMsg = "Can not assign teacher to course due to, teacher already assigned to 3 courses this semester";
                return View(courseData);
            }    

            var res = await _courseService.CreateCourse(courseData);
            setUserMsg(res);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteCourse(string id){
           var res = await _courseService.DeleteCourse(id);
           setUserMsg(res);
           return RedirectToAction("Index");
        } 

        [HttpPost]
        public IActionResult SetCourseStudentNumber(AllCoursesVM data){
            //adjusting extremes
            if (data.StudentNumberModel.maximumstudents < -1) data.StudentNumberModel.maximumstudents = -1;
            if (data.StudentNumberModel.maximumstudents > 1000) data.StudentNumberModel.maximumstudents = 1000;
            return RedirectToAction("Allcourses", new {data.StudentNumberModel.maximumstudents});
        }

        [HttpGet]
        public async Task<IActionResult> AllCourses(int maximumstudents){
            var models = await _courseService.GetAllCoursesAsync(maximumstudents);
            models = models.Where(x=> x.Archived == false).ToList();
            var modelstub = new CourseStudentNumberModel();
            var viewModel = new AllCoursesVM(){courseList = models, StudentNumberModel=modelstub};
            return View(viewModel);
        }

        public async Task<IActionResult> EditCourse(string id){
            var model = await _courseService.GetCourseAsync(id);
            return View(model);
        }

        [HttpPost]
        public async Task <IActionResult> EditCourse(CourseModel courseData){
            if(!ModelState.IsValid){
                return View(courseData);
            }
            var res = await _courseService.UpdateCourse(courseData);
            setUserMsg(res);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> TemplateCourse(string id){
            var model = await _courseService.GetCourseAsync(id);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> TemplateCourse(CourseModel courseData){
            if(!ModelState.IsValid){
                return View(courseData);
            }
            var res = await _courseService.CreateCourse(courseData);
            setUserMsg(res);
            return RedirectToAction("Index");
        }


        public async Task<IActionResult> BulkDeleteCourses(){
            var res = await _courseService.BulkDeleteCourses();
            TempData["cc"] = "Success - The operation completed. Number of courses removed : " + res.ToString();
            return RedirectToAction("Index");
        } 


        private void setUserMsg(bool flag){
            if(flag){
                TempData["cc"]="Success - The operation completed";
            }else
            {
                TempData["cc"]="Error - The operation did not complete";
            }
        }

    }
}