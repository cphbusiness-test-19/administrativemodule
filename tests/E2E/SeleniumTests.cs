using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace Tests
{
    [Category("e2e")]
    [Category("SeleniumTests")]
    [Category("slow")]
    public class SeleniumTests
    {
        IWebDriver wdriver;
        DirectoryInfo DI;
        string rootDir,validUserName,validPassword;

        [SetUp]
        public void setup(){
            validUserName="test";
            validPassword="test";
            var options = new FirefoxOptions();
            options.AddArguments("--headless");

            //DI = new DirectoryInfo(Directory.GetCurrentDirectory()); 
            //rootDir = DI.Parent.Parent.Parent.FullName;
            //wdriver = new FirefoxDriver(Directory.GetCurrentDirectory());
             
            string GridURL = "http://ssfirefox:4444/wd/hub";
            wdriver = new RemoteWebDriver(new Uri(GridURL), options);
        }

        [TearDown]
        public void Tesrdown(){
            wdriver.Quit();
        }


        [Test]
        public void smalltest(){
            Assert.Pass();
        }

        [Test]
        public void Navigate_to_loginPage_returnsLoginPage(){
            wdriver.Navigate().GoToUrl("http://administrativemodule:80");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Home"));

            wdriver.FindElement(By.XPath("//a[@href='/Login/Login']")).Click();

            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Login"));

            var actual = wdriver.Title.Contains("Login");
            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/Navigate_to_loginPage_returnsLoginPage.png", ScreenshotImageFormat.Png);

            Assert.That(actual, Is.True);
            
        }


        [Test]
        public void Try_login_with_only_username_returnsApproriateMsg(){
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/login/login");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Login"));

            var element = wdriver.FindElement(By.Id("UserName"));
            element.SendKeys("someUserName");

            wdriver.FindElement(By.Id("submit_id")).Click();
            
            var actual = wdriver.FindElement(By.Id("password_validation_id")).Text;

            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/Try_login_with_only_username_returnsApproriateMsg.png", ScreenshotImageFormat.Png);

            Assert.That(actual, Is.EqualTo("The Password field is required."));
            
        }

        [Test]
        public void Try_login_with_only_password_returnsApproriateMsg(){
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/login/login");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Login"));

            var element = wdriver.FindElement(By.Id("Password"));
            element.SendKeys("somePassWord");

            wdriver.FindElement(By.Id("submit_id")).Click();
            
            var actual = wdriver.FindElement(By.Id("username_validation_id")).Text;

            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/Try_login_with_only_password_returnsApproriateMsg.png", ScreenshotImageFormat.Png);

            Assert.That(actual, Is.EqualTo("The UserName field is required."));
            
        }

        [Test]
        public void Try_login_with_wrong_credentials_returnsApproriateMsg(){
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/login/login");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Login"));

            var element = wdriver.FindElement(By.Id("UserName"));
            element.SendKeys("someUsername");

            element = wdriver.FindElement(By.Id("Password"));
            element.SendKeys("somePassWord");

            wdriver.FindElement(By.Id("submit_id")).Click();
            
            //wait for microservice to respond
            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Login"));

            var actual = wdriver.FindElement(By.Id("message_id")).Text;

            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/Try_login_with_wrong_credentials_returnsApproriateMsg.png", ScreenshotImageFormat.Png);

            Assert.That(actual, Is.EqualTo("Credentials incorrect"));
        }

        [Test]
        public void Try_login_with_correct_credentials_returnsLandingPage(){
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/login/login");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Login"));

            var element = wdriver.FindElement(By.Id("UserName"));
            element.SendKeys(validUserName);

            element = wdriver.FindElement(By.Id("Password"));
            element.SendKeys(validPassword);

            wdriver.FindElement(By.Id("submit_id")).Click();
            
            //wait for microservice to respond
            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Landing"));

            var actual = wdriver.Title.Contains("Landing");

            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/Try_login_with_correct_credentials_returnsLandingPage.png", ScreenshotImageFormat.Png);

            Assert.That(actual, Is.True);
        }

        [Test]
        public void Navigate_toCourseModule_returnCourseModulePage(){
            //setup
            loginbeforeTesting();
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/login/landing");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Landing"));

            wdriver.FindElement(By.Id("2")).Click();
            
            //wait for microservice to respond
            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Managing courses"));

            var actual = wdriver.Title.Contains("Managing courses");

            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/Navigate_toCourseModule_returnCourseModulePage.png", ScreenshotImageFormat.Png);

            Assert.That(actual, Is.True);
        }

        [Test]
        public void NavigateTo_CreateNew_Course_ReturnsNewCoursePage(){
            //setup
            loginbeforeTesting();
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/Course");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("courses"));

            wdriver.FindElement(By.XPath("//a[@href='/Course/CreateCourse']")).Click();
            
            //wait for microservice to respond
            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Create course"));

            var actual = wdriver.Title.Contains("Create course");

            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/NavigateTo_CreateNew_Course_ReturnsNewCoursePage.png", ScreenshotImageFormat.Png);

            Assert.That(actual, Is.True);
        }

        [Test]
        public void CreateNew_Course_ReturnsNewCourseInList(){
            //setup
            loginbeforeTesting();
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/Course/CreateCourse");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Create course"));

            var element = wdriver.FindElement(By.Id("CourseName"));
            element.SendKeys("SomeCourseName");
            //submit form with only name set
            element.Submit();
            //wait for microservice to respond
            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Managing courses"));

            wdriver.Navigate().GoToUrl("http://administrativemodule:80/Course/AllCourses");

            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("All courses"));
            
            var elementList = wdriver.FindElements(By.ClassName("course-container"));

            var actual = false;
            foreach (var item in elementList)
            {
                if(item.FindElement(By.ClassName("course-name-container")).Text.Contains("SomeCourseName")){
                    actual=true; //element found
                    // Actions actions = new Actions(wdriver);
                    // actions.MoveToElement(item);
                    // actions.Perform();

                    /* The above MoveToElement is unsupported in firefox at this time,
                    *  in that the element(item) found in the list is outside the viewport
                    * 
                    *  https://github.com/spyoungtech/behave-webdriver/issues/41
                    *   
                    *   "...For now, we will consider moving with too large offset unsupported for firefox. 
                    *   This scenario has been marked with the @skip_firefox tag."
                    */

                    //documentation of the list, but sadly not the actual element in the list
                    Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
                    ss.SaveAsFile(@"../artifact/CreateNew_Course_ReturnsNewCourseInList.png", ScreenshotImageFormat.Png);
                }
            }
            //the assert is still possitive, which means the element is indeed pressent in the list, just not screenshot to prove it :(
            Assert.That(actual, Is.True);
        }

        [Test]
        public void DeleteCourse_ReturnsListwithoutCourse(){
            //setup
            loginbeforeTesting();
            string courseName = "SomecourseName1234";
            //Create a course to be deleted
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/Course/CreateCourse");
            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Create course"));

            var element = wdriver.FindElement(By.Id("CourseName"));
            element.SendKeys(courseName);
            //submit form with only name set
            element.Submit();

            wdriver.Navigate().GoToUrl("http://administrativemodule:80/Course/AllCourses");

            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("All courses"));

            
            var elementList = wdriver.FindElements(By.ClassName("course-container"));

            foreach (var item in elementList)
            {
                if(item.FindElement(By.ClassName("course-name-container")).Text.Contains(courseName)){
                    item.FindElement(By.Id("delete_id")).Click();
                }
            }
            //open list with all courses
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/Course/AllCourses");

            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("All courses"));


            //preventing stale reference, by forcing "re-DOM"
            wdriver.Navigate().Refresh();
            //search the list to confirm deletion
            elementList = wdriver.FindElements(By.ClassName("course-container"));
            //assume not found
            var actual = true;
            foreach (var item in elementList)
            {
                if(item.FindElement(By.ClassName("course-name-container")).Text.Contains(courseName)){
                    actual=false; // course not deleted <- should not end up here 
                }
            }
            //diffecult to take screenshot of something not there, so the list will do
            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/DeleteCourse_ReturnsListwithoutCourse.png", ScreenshotImageFormat.Png);
            Assert.That(actual, Is.True);
        }

        [Test]
        public void ShowAllCourse_returnsListwithAllCourses(){
            loginbeforeTesting();
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/login/landing");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Landing"));

            wdriver.FindElement(By.Id("2")).Click();
            
            //wait for microservice to respond
            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Managing courses"));

           //open list with all courses
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/Course/AllCourses");

            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("All courses"));

            var actual = wdriver.Title.Contains("All courses");

            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/ShowAllCourse_returnsListwithAllCourses.png", ScreenshotImageFormat.Png);
            Assert.That(actual, Is.True);
        }

        [Test]
        public void BulkDeleteCourses_RemovesCoursesFromList_ReturnsNumberRemoved(){
            loginbeforeTesting();
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/login/landing");

            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Landing"));

            wdriver.FindElement(By.Id("2")).Click();
            
            //wait for microservice to respond
            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Managing courses"));

           //open list with all courses
            wdriver.FindElement(By.XPath("//a[@href='/Course/BulkDeleteCourses']")).Click();

            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Managing courses"));

            var actual = wdriver.Title.Contains("Managing courses");
            var userMsg = wdriver.FindElement(By.Id("userMsg_id")).Text.Contains("Success");

            Screenshot ss = ((ITakesScreenshot)wdriver).GetScreenshot();
            ss.SaveAsFile(@"../artifact/BulkDeleteCourses_RemovesCoursesFromList_ReturnsNumberRemoved.png", ScreenshotImageFormat.Png);
            Assert.That(actual, Is.True);
            Assert.That(userMsg, Is.True);
        }


       private void loginbeforeTesting(){
            wdriver.Navigate().GoToUrl("http://administrativemodule:80/login/login");
            var wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Login"));

            var element = wdriver.FindElement(By.Id("UserName"));
            element.SendKeys(validUserName);

            element = wdriver.FindElement(By.Id("Password"));
            element.SendKeys(validPassword);

            wdriver.FindElement(By.Id("submit_id")).Click();
            
            //wait for microservice to respond
            wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.Title.Contains("Landing"));

        }

    }
}