using System.Threading.Tasks;

namespace module
{
    public interface IAuthService
    {
        Task<UserModel> VerifyCredentials(UserModel userData);
        
    }
}