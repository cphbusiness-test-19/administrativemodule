using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using module;
using Moq;
using NUnit.Framework;

namespace Tests
{
    [Category("unit-test")]
    [Category("CourseController")]
    [Category("fast")]
    public class CourseControllerTests
    {

        Mock<ICourseService> mockCourseService;
        CourseModel courseData;
        DefaultHttpContext httpContext;
        TempDataDictionary tempData;
        AllCoursesVM allCourseVM;
        CourseStudentNumberModel courseStudentNumberModel;

        [SetUp]
        public void setup(){
            courseStudentNumberModel = new CourseStudentNumberModel();
            mockCourseService = new Mock<ICourseService>();
            courseData = new CourseModel();
            allCourseVM = new AllCoursesVM();
            httpContext = new DefaultHttpContext();
            tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
        }

        [Test]
        public void smalltest(){
            Assert.Pass();
        }

        [Test]
        public void Index_UpdateViewData_returnsViewResult(){
            string viewdataString = "someString";
            var courseService = mockCourseService.Object;
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData = tempData};

            var actual = controller.Index();
            var viewResult = actual as ViewResult;

            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "Index action in CourseController does not return a ViewResult");
            Assert.That(viewResult.ViewData["userMsg"], Is.EqualTo(viewdataString));
        }
    
        [Test]
        public void CreateCourse_returnsViewResult(){

            var courseService = mockCourseService.Object;

            var actual = new CourseController(courseService).CreateCourse();

            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "CreateCourse action does not return ViewResult");
        }
    
        [Test]
        public async Task CreateCourse_InvalidModel_returnsViewResult(){

            var validationResults = new List<ValidationResult>();
            var courseService = mockCourseService.Object;

            var controller = new CourseController(courseService);
            //inject error
            controller.ModelState.AddModelError("CourseName", "Required");
            var actual = await controller.CreateCourse(courseData);
            //perform modelvalidation
            var modelValidation = Validator.TryValidateObject(courseData, new ValidationContext(courseData),validationResults);
            
            Assert.IsFalse(modelValidation);
            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "CreateCourse action with invalid model does not return ViewResult");
        }

        [Test]
        public async Task CreateCourse_ValidModel_returnsRedirectResult(){
            var respList = new List<CourseModel>(){
                new CourseModel(){TeacherId=123, Semester="2019_Fall"}
            };
            courseData.TeacherId=123;
            courseData.Semester="2019_Fall";

            mockCourseService.Setup(x=>x.CreateCourse(courseData)).Returns(Task.FromResult(true));
            mockCourseService.Setup(x =>x.GetAllCoursesAsync(0)).Returns(Task.FromResult(respList));
            var courseService = mockCourseService.Object;
            string viewdataString = "Success - The operation completed";
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.CreateCourse(courseData);

            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "CreateCourse valid model, does not return a redirect-to-action-result");
        }


        [Test]
        public async Task CreateCourse_ValidModel_failedmicroservice_returnsRedirectResult(){
            var respList = new List<CourseModel>(){
                new CourseModel(){TeacherId=123, Semester="2019_Fall"}
            };
            courseData.TeacherId=123;
            courseData.Semester="2019_Fall";

            mockCourseService.Setup(x=>x.CreateCourse(courseData)).Returns(Task.FromResult(false));
            mockCourseService.Setup(x =>x.GetAllCoursesAsync(0)).Returns(Task.FromResult(respList));
            var courseService = mockCourseService.Object;
            string viewdataString = "Error, The operation did not complete";
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.CreateCourse(courseData);

            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "CreateCourse valid model,- failed microservice - does not return a redirect-to-action-result");            
        }

        [Test]
        public async Task CreateCourse_ValidModel_sameTeacherIDAndSemester4times_returnsViewResult(){
            var respList = new List<CourseModel>(){
                new CourseModel(){TeacherId=123, Semester="2019_Fall"},
                new CourseModel(){TeacherId=123, Semester="2019_Fall"},
                new CourseModel(){TeacherId=123, Semester="2019_Fall"},
                new CourseModel(){TeacherId=123, Semester="2019_Fall"}
            };
            courseData.TeacherId=123;
            courseData.Semester="2019_Fall";

            mockCourseService.Setup(x=>x.CreateCourse(courseData)).Returns(Task.FromResult(true));
            mockCourseService.Setup(x =>x.GetAllCoursesAsync(0)).Returns(Task.FromResult(respList));
            var courseService = mockCourseService.Object;

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.CreateCourse(courseData);
            var result = actual as ViewResult;

            Assert.NotNull(actual);
            Assert.That(result.ViewData["ErrMsg"], Is.EqualTo("Can not assign teacher to course due to, teacher already assigned to 3 courses this semester"));
            Assert.IsInstanceOf<ViewResult>(actual, "CreateCourse valid model, Same teacherID used more than 3 times same semester should not be possible");            
        }


        [Test]
        public async Task DeleteCourse_MicroserviceSuccess_returnsRedirectResult(){

            mockCourseService.Setup(x=>x.DeleteCourse("42")).Returns(Task.FromResult(true));
            var courseService = mockCourseService.Object;
            string viewdataString = "Success - The operation completed";
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.DeleteCourse("42");

            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "DeleteCourse action - MS success - does not return ViewResult");
        }

        [Test]
        public async Task DeleteCourse_MicroserviceFail_returnsRedirectResult(){

            mockCourseService.Setup(x=>x.DeleteCourse("42")).Returns(Task.FromResult(false));
            var courseService = mockCourseService.Object;
            string viewdataString = "Error, The operation did not complete";
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.DeleteCourse("42");

            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "DeleteCourse action - MS fail - does not return ViewResult");
        }

        [Test]
        public async Task EditCourse_MicroserviceSuccess_returnsViewResult(){

            mockCourseService.Setup(x=>x.GetCourseAsync("42")).Returns(Task.FromResult(courseData));
            var courseService = mockCourseService.Object;
            string viewdataString = "Success - The operation completed";
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.EditCourse("42");
            var result = actual as ViewResult;

            Assert.NotNull(actual);
            Assert.That(result.TempData["cc"], Is.EqualTo(viewdataString));
            Assert.IsInstanceOf<ViewResult>(actual, "EditCourse action - MS success - does not return ViewResult");
        }

        [Test]
        public async Task EditCourse_MicroserviceFail_returnsViewResult(){

            mockCourseService.Setup(x=>x.GetCourseAsync("42")).Returns(Task.FromResult(courseData));
            var courseService = mockCourseService.Object;
            string viewdataString = "Error, The operation did not complete";
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.EditCourse("42");
            var result = actual as ViewResult;

            Assert.NotNull(actual);
            Assert.That(result.TempData["cc"], Is.EqualTo(viewdataString));
            Assert.IsInstanceOf<ViewResult>(actual, "EditCourse action - MS fail - does not return ViewResult");
        }

        [Test]
        public async Task EditCourse_inValidModel_ReturnsViewResult(){
            var validationResults = new List<ValidationResult>();
            var courseService = mockCourseService.Object;

            var controller = new CourseController(courseService);
            //inject error
            controller.ModelState.AddModelError("CourseName", "Required");
            var actual = await controller.EditCourse(courseData);
            //perform modelvalidation
            var modelValidation = Validator.TryValidateObject(courseData, new ValidationContext(courseData),validationResults);
            
            Assert.IsFalse(modelValidation);
            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "EditCourse action with invalid model does not return ViewResult");
        }

        [Test]
        public async Task EditCourse_ValidModel_ReturnsRedirectResult(){
            mockCourseService.Setup(x=>x.UpdateCourse(courseData)).Returns(Task.FromResult(true));
            var courseService = mockCourseService.Object;
            string viewdataString = "Success - The operation completed";
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.EditCourse(courseData);

            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "EditCourse valid model, does not return a redirect-to-action-result");
        }

        [Test]
        public async Task BulkDeleteCourse_returnsRedirectResult(){
            int someNumber = 42;
            mockCourseService.Setup(x => x.BulkDeleteCourses()).Returns(Task.FromResult(someNumber));

            var courseService = mockCourseService.Object;
            string viewdataString = "Success - The operation completed. Number of courses removed : " + someNumber.ToString();
            tempData["cc"] = viewdataString;        

            var controller = new CourseController(courseService){TempData=tempData};
            var actual = await controller.EditCourse(courseData);

            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "BulkDelete, does not return a redirect-to-action-result");
        }


        [Test]
        public void SetCourseStudentNumber_Validmodel_studentnumberCorrect_returnsRedirectResult(){
            courseStudentNumberModel.maximumstudents=42;
            courseStudentNumberModel.Err="";
            allCourseVM.StudentNumberModel = courseStudentNumberModel;

            var validationResults = new List<ValidationResult>();
            var courseService = mockCourseService.Object;

            var controller = new CourseController(courseService);
            var actual = controller.SetCourseStudentNumber(allCourseVM) as RedirectToActionResult;
            

            //perform modelvalidation
            var modelValidation = Validator.TryValidateObject(allCourseVM, new ValidationContext(allCourseVM),validationResults);
            var expected = new RouteValueDictionary{
                {"maximumstudents", courseStudentNumberModel.maximumstudents}
            };

            Assert.That(actual.RouteValues, Is.EqualTo(expected));
            Assert.True(modelValidation);
            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "SetCourseStudentNumber- Valid model, does not return a redirect-to-action-result");
        }


        [Test]
        public void SetCourseStudentNumber_Validmodel_studentnumberBelowLimit_returnsRedirectResult(){
            courseStudentNumberModel.maximumstudents=-2;
            courseStudentNumberModel.Err="";
            allCourseVM.StudentNumberModel = courseStudentNumberModel;

            var validationResults = new List<ValidationResult>();
            var courseService = mockCourseService.Object;

            var controller = new CourseController(courseService);
            var actual = controller.SetCourseStudentNumber(allCourseVM) as RedirectToActionResult;
            

            //perform modelvalidation
            var modelValidation = Validator.TryValidateObject(allCourseVM, new ValidationContext(allCourseVM),validationResults);
            var expected = new RouteValueDictionary{
                {"maximumstudents", -1}
            };

            Assert.That(actual.RouteValues, Is.EqualTo(expected));
            Assert.True(modelValidation);
            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "SetCourseStudentNumber- Valid model, does not return a redirect-to-action-result");
        }

        [Test]
        public void SetCourseStudentNumber_Validmodel_studentnumberAboveLimit_returnsRedirectResult(){
            courseStudentNumberModel.maximumstudents=1001;
            courseStudentNumberModel.Err="";
            allCourseVM.StudentNumberModel = courseStudentNumberModel;

            var validationResults = new List<ValidationResult>();
            var courseService = mockCourseService.Object;

            var controller = new CourseController(courseService);
            var actual = controller.SetCourseStudentNumber(allCourseVM) as RedirectToActionResult;
            

            //perform modelvalidation
            var modelValidation = Validator.TryValidateObject(allCourseVM, new ValidationContext(allCourseVM),validationResults);
            var expected = new RouteValueDictionary{
                {"maximumstudents", 1000}
            };

            Assert.That(actual.RouteValues, Is.EqualTo(expected));
            Assert.True(modelValidation);
            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual, "SetCourseStudentNumber- Valid model, does not return a redirect-to-action-result");
        }

        [Test]
        public async Task AllCourses_returnsViewResult(){
            int studentNumber=42;
            List<CourseModel> responseList = new List<CourseModel>(){
                new CourseModel(){Archived=true},
                new CourseModel(){Archived=false},
                new CourseModel(){Archived=true},
            };
            mockCourseService.Setup(x => x.GetAllCoursesAsync(studentNumber)).Returns(Task.FromResult(responseList));
            var courseService = mockCourseService.Object;
            var controller = new CourseController(courseService);

            var actual = await controller.AllCourses(studentNumber);

            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "AllCourses - does not return a ViewResult");
        }

        [Test]
        public async Task AllCourses_removesArchived_returnsViewResult(){
            int studentNumber=42;
            List<CourseModel> responseList = new List<CourseModel>(){
                new CourseModel(){Archived=true},
                new CourseModel(){Archived=false},
                new CourseModel(){Archived=true},
            };
            mockCourseService.Setup(x=>x.GetAllCoursesAsync(studentNumber)).Returns(Task.FromResult(responseList));
            var courseService = mockCourseService.Object;
            var controller = new CourseController(courseService);

            var actual = await controller.AllCourses(studentNumber) as ViewResult;
            var VM = actual.Model as AllCoursesVM;

            Assert.NotNull(actual);
            Assert.That(VM.courseList.Count, Is.EqualTo(1));
            Assert.IsInstanceOf<ViewResult>(actual, "AllCourses - does not return a ViewResult");
        }

        [Test]
        public async Task AllCourses_removesArchived_allArchived_returnsViewResult(){
            int studentNumber=42;
            List<CourseModel> responseList = new List<CourseModel>(){
                new CourseModel(){Archived=true},
                new CourseModel(){Archived=true},
                new CourseModel(){Archived=true},
            };
            mockCourseService.Setup(x=>x.GetAllCoursesAsync(studentNumber)).Returns(Task.FromResult(responseList));
            var courseService = mockCourseService.Object;
            var controller = new CourseController(courseService);

            var actual = await controller.AllCourses(studentNumber) as ViewResult;
            var VM = actual.Model as AllCoursesVM;

            Assert.NotNull(actual);
            Assert.That(VM.courseList.Count, Is.EqualTo(0));
            Assert.IsInstanceOf<ViewResult>(actual, "AllCourses - does not return a ViewResult");
        }

        [Test]
        public async Task TemplateCourse_returnsViewResult(){
            string id = "someID";
            mockCourseService.Setup(x => x.GetCourseAsync(id)).Returns(Task.FromResult(courseData));
            var courseService = mockCourseService.Object;
            var controller = new CourseController(courseService);

            var actual = await controller.TemplateCourse(id) as ViewResult;

            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "TemplateCourse - does not return a ViewResult");
        }

        [Test]
        public async Task TemplateCourse_ReuseData_returnsViewResult(){
            string id = "someID";
            string templateName = "tempName";
            courseData = new CourseModel(){Id=id,CourseName=templateName};
            mockCourseService.Setup(x => x.GetCourseAsync(id)).Returns(Task.FromResult(courseData));
            var courseService = mockCourseService.Object;
            var controller = new CourseController(courseService);

            var actual = await controller.TemplateCourse(id) as ViewResult;
            var VM = actual.Model as CourseModel;

            Assert.NotNull(actual);
            Assert.That(VM.Id, Is.EqualTo(id));
            Assert.That(VM.CourseName, Is.EqualTo(templateName));
            Assert.IsInstanceOf<ViewResult>(actual, "TemplateCourse - does not return a ViewResult");
        }

        [Test]
        public void teacherHasToManyCourses_BVtest2_returnsFalse(){
            var respList = new List<CourseModel>(){
                new CourseModel(){TeacherId=42, Semester="2019_Fall"},
                new CourseModel(){TeacherId=42, Semester="2019_Fall"}
            };
            courseData.TeacherId=42;
            courseData.Semester="2019_Fall";

            mockCourseService.Setup(x=>x.CreateCourse(courseData)).Returns(Task.FromResult(true));
            mockCourseService.Setup(x =>x.GetAllCoursesAsync(0)).Returns(Task.FromResult(respList));
            var courseService = mockCourseService.Object;
            var controller = new CourseController(courseService);
            var actual = controller.teacherHasToManyCourses(courseData.TeacherId, respList, courseData.Semester);

            Assert.False(actual);
        }

        [Test]
        public void teacherHasToManyCourses_BVtest3_returnsFalse(){
            var respList = new List<CourseModel>(){
                new CourseModel(){TeacherId=42, Semester="2019_Fall"},
                new CourseModel(){TeacherId=42, Semester="2019_Fall"},
                new CourseModel(){TeacherId=42, Semester="2019_Fall"}
            };
            courseData.TeacherId=42;
            courseData.Semester="2019_Fall";

            mockCourseService.Setup(x=>x.CreateCourse(courseData)).Returns(Task.FromResult(true));
            mockCourseService.Setup(x =>x.GetAllCoursesAsync(0)).Returns(Task.FromResult(respList));
            var courseService = mockCourseService.Object;
            var controller = new CourseController(courseService);
            var actual = controller.teacherHasToManyCourses(courseData.TeacherId, respList, courseData.Semester);

            Assert.True(actual);
        }


        [Test]
        public void teacherHasToManyCourses_BVtest4_returnsTrue(){
            var respList = new List<CourseModel>(){
                new CourseModel(){TeacherId=42, Semester="2019_Fall"},
                new CourseModel(){TeacherId=42, Semester="2019_Fall"},
                new CourseModel(){TeacherId=42, Semester="2019_Fall"},
                new CourseModel(){TeacherId=42, Semester="2019_Fall"}
            };
            courseData.TeacherId=42;
            courseData.Semester="2019_Fall";

            mockCourseService.Setup(x=>x.CreateCourse(courseData)).Returns(Task.FromResult(true));
            mockCourseService.Setup(x =>x.GetAllCoursesAsync(0)).Returns(Task.FromResult(respList));
            var courseService = mockCourseService.Object;
            var controller = new CourseController(courseService);
            var actual = controller.teacherHasToManyCourses(courseData.TeacherId, respList, courseData.Semester);

            Assert.True(actual);
        }


    }


}