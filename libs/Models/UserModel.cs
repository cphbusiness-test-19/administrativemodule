using System.ComponentModel.DataAnnotations;

namespace module
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool Public { get; set; }
        public bool Active { get; set; }
        public string Token { get; set; }
        public string Title { get; set; }
        public string httpres { get; set; }
    }
}