using System.Threading.Tasks;
using module;
using Moq;
using NUnit.Framework;
//using System.Net.Http;
using Moq.Protected;
using System.Threading;
//using System.Net;
using System;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Text;
using System.Net.Http;
using System.Net;

namespace Tests
{
    [Category("unit-test")]
    [Category("fast")]
    public class AuthServiceTests
    {
        UserModel userData;
        AuthService authService;
        Mock<HttpMessageHandler> handlerMock;

        [SetUp]
        public void setup(){
            userData = new UserModel();
            handlerMock=null;
        }



        [Test]
        public async Task VerifyCredentials_ValidCredentials_UpdatesUserdataWithToken(){
            
            userData.UserName="validname";
            userData.Password="validpass";
            
            string expected = "SomeEncryptedToken";
            var JsonResponse = "{ \"access_token\": \""+ expected + "\"}"; 


            var fakeHttpMessageHandler = new MockHttpClientResponse(new HttpResponseMessage() 
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonResponse, Encoding.UTF8, "application/json") 
            });

            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);
            var mockFactory = new Mock<IHttpClientFactory>();
            mockFactory.Setup(x=>x.CreateClient(It.IsAny<string>())).Returns(fakeHttpClient);


            var factory = mockFactory.Object;
            authService = new AuthService(factory);
            var actual = await authService.VerifyCredentials(userData);

            mockFactory.Verify(x=>x.CreateClient(It.IsAny<string>()));
            Assert.NotNull(actual);
            Assert.NotNull(authService);
            Assert.That(actual.Token, Is.EqualTo(expected));

        }

        [Test]
        public async Task VerifyCredentials_InValidCredentials_SetUserDataToNull(){
            
            userData.UserName="Invalidname";
            userData.Password="Invalidpass";
            
            string expected = "";
            var JsonResponse = "{ \"error\": \""+ expected + "\"}";  

            var fakeHttpMessageHandler = new MockHttpClientResponse(new HttpResponseMessage() 
            {
                StatusCode = HttpStatusCode.Unauthorized,
                Content = new StringContent(JsonResponse, Encoding.UTF8, "application/json") 
            });

            var fakeHttpClient = new HttpClient(fakeHttpMessageHandler);
            var mockFactory = new Mock<IHttpClientFactory>();
            mockFactory.Setup(x=>x.CreateClient(It.IsAny<string>())).Returns(fakeHttpClient);

            var factory = mockFactory.Object;
            authService = new AuthService(factory);
            var actual = await authService.VerifyCredentials(userData);

            mockFactory.Verify(x=>x.CreateClient(It.IsAny<string>()));
            Assert.Null(actual);
            Assert.NotNull(authService);
        }





    }
}