using System.Collections.Generic;

namespace module
{
    public class AllCoursesVM
    {
        public List<CourseModel> courseList { get; set; }
        public CourseStudentNumberModel StudentNumberModel { get; set; }
    }
}