using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

//courseModel without the Id field, because javascript <> c#
namespace module
{
        public class smallCourseModel{
        public string CourseName { get; set; }
        public string Room { get; set; }
        public int TeacherId { get; set; } = 0;
        public List<int> Students { get; set; }
        public string Category { get; set; }
        public int NoHoursPrWeek { get; set; } = 0;
        public string Weekdays { get; set; }
        public string Description { get; set; }
        public string Semester { get; set; }
        public string Language { get; set; }
        public string Level { get; set; }
        public bool Visible { get; set; } =false;
        public bool Archived { get; set; } =false;
    }
}