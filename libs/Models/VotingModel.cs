using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace module
{
    public class VotingModel
    {
        public string Id { get; set; }
        public bool Active { get; set; }
        public List<CourseModel> Courses { get; set; } 
        public DateTime CreatedAt { get; set; }
        
    }
}