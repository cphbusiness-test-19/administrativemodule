using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using module;
using Moq;
using NUnit.Framework;

namespace Tests
{
    [Category("unit-test")]
    [Category("fast")]
    public class LoginServiceTests
    {
        IAuthService authService;
        Mock<IAuthService> mockAuthService;
        ICookieManager cookieManager;
        Mock<ICookieManager> mockCookieMgr;
        UserModel userData;


        [SetUp]
        public void setup(){
            //mock up dependencies
            mockAuthService = new Mock<IAuthService>();
            mockCookieMgr  = new Mock<ICookieManager>();
            userData = new UserModel();
        }

        [Test]
        public void smallTest(){
            Assert.Pass();
        }
        
        [Test]
        public void Login_returnViewResult(){

            authService = mockAuthService.Object;
            cookieManager = mockCookieMgr.Object;

            var actual = new LoginController(authService, cookieManager).Login();
            Assert.IsInstanceOf<ViewResult>(actual,"Login does not return viewresult");
        }

        [Test]
        public void Logout_AdjustsCookies_ShouldcallDeleteAndAppendOnce(){

            mockCookieMgr.Setup(x=>x.Append(It.IsAny<string>(),It.IsAny<string>(),It.IsAny<CookieOptions>(),It.IsAny<HttpResponse>()));
            mockCookieMgr.Setup(x=>x.Delete(It.IsAny<string>(),It.IsAny<HttpResponse>()));

            authService = mockAuthService.Object;
            cookieManager = mockCookieMgr.Object;
    
            var actual = new LoginController(authService, cookieManager).Logout();
            mockCookieMgr.Verify(x=>x.Delete(It.IsAny<string>(), It.IsAny<HttpResponse>()), Times.Once);
            mockCookieMgr.Verify(y=>y.Append(It.IsAny<string>(),It.IsAny<string>(),It.IsAny<CookieOptions>(),It.IsAny<HttpResponse>()), Times.Once);

        }

        [Test]
        public void Logout_ReturnsRedirectResult(){

            mockCookieMgr.Setup(x=>x.Append(It.IsAny<string>(),It.IsAny<string>(),It.IsAny<CookieOptions>(),It.IsAny<HttpResponse>()));
            mockCookieMgr.Setup(x=>x.Delete(It.IsAny<string>(),It.IsAny<HttpResponse>()));

            authService = mockAuthService.Object;
            cookieManager = mockCookieMgr.Object;

            var actual = new LoginController(authService, cookieManager).Logout();
            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectResult>(actual, "Logout does not return a redirect result");
        }

        [Test]
        public async Task Login_BadModelState_noUserNameAndPW_returnsView(){

            authService = mockAuthService.Object;
            cookieManager = mockCookieMgr.Object;
            var validationResults = new List<ValidationResult>();

            var controller = new LoginController(authService,cookieManager);
            // inject modelerror
            controller.ModelState.AddModelError("UserName", "Required");
            var actual = await controller.Login(userData);
            var modelValidation = Validator.TryValidateObject(userData, new ValidationContext(userData),validationResults);
            
            Assert.IsFalse(modelValidation);
            Assert.That(validationResults.Count, Is.EqualTo(2));
            Assert.That("UserName", Is.EqualTo(validationResults[0].MemberNames.ElementAt(0)));
            Assert.That("Password", Is.EqualTo(validationResults[1].MemberNames.ElementAt(0)));
            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "Invalid modelstate does not return a viewResult");

        }

        [Test]
        public async Task Login_BadModelState_noUserNameAndValidPassword_returnsView(){

            authService = mockAuthService.Object;
            cookieManager = mockCookieMgr.Object;
            var validationResults = new List<ValidationResult>();
            userData.Password="RandomPW";
            var controller = new LoginController(authService,cookieManager);
            // inject modelerror
            controller.ModelState.AddModelError("UserName", "Required");

            var actual = await controller.Login(userData);
            var modelValidation = Validator.TryValidateObject(userData, new ValidationContext(userData),validationResults);
            
            Assert.IsFalse(modelValidation);
            Assert.That(validationResults.Count, Is.EqualTo(1));
            Assert.That("UserName", Is.EqualTo(validationResults[0].MemberNames.ElementAt(0)));
            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "Invalid modelstate does not return a viewResult");

        }

        [Test]
        public async Task Login_BadModelState_ValidUserNameAndNoPassword_returnsView(){

            authService = mockAuthService.Object;
            cookieManager = mockCookieMgr.Object;
            
            var validationResults = new List<ValidationResult>();
            userData.UserName="RandomUserName";
            var controller = new LoginController(authService,cookieManager);
            // inject modelerror
            controller.ModelState.AddModelError("Password", "Required");

            var actual = await controller.Login(userData);
            var modelValidation = Validator.TryValidateObject(userData, new ValidationContext(userData),validationResults);
            
            Assert.IsFalse(modelValidation);
            Assert.That(validationResults.Count, Is.EqualTo(1));
            Assert.That("Password", Is.EqualTo(validationResults[0].MemberNames.ElementAt(0)));
            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "Invalid modelstate does not return a viewResult");

        }

        [Test]
        public async Task Login_validModelState_InvalidUser_returnsView(){

            authService = mockAuthService.Object;
            cookieManager = mockCookieMgr.Object;
            
            var validationResults = new List<ValidationResult>();
            userData.UserName="RandomUserName";
            userData.Password="RandomUserPW";
            var controller = new LoginController(authService,cookieManager);

            var actual = await controller.Login(userData);


            var modelValidation = Validator.TryValidateObject(userData, new ValidationContext(userData),validationResults);
            
            Assert.IsTrue(modelValidation);
            Assert.That(controller.ViewData["message"], Is.EqualTo("Credentials incorrect"));
            Assert.NotNull(actual);
            Assert.IsInstanceOf<ViewResult>(actual, "valid modelstate invalidUser does not return a viewResult");

        }

        [Test]
        public async Task Login_validModelState_ValidUser_returnsRedirectAction(){
            //userdata going in
            userData.UserName="q";
            userData.Password="q";
            
            //userData going out
            UserModel newUD = new UserModel(){
                UserName = userData.UserName,
                Password = userData.Password,
                Token = "someTokenstringFromMicroService"
            };

            //setup mockAuthService to return valid userData;
            mockAuthService.Setup(x=>x.VerifyCredentials(userData)).Returns(Task.FromResult(newUD));
            authService = mockAuthService.Object;
            cookieManager = mockCookieMgr.Object;
            
            var validationResults = new List<ValidationResult>();
            var controller = new LoginController(authService,cookieManager);

            var actual = await controller.Login(userData);


            var modelValidation = Validator.TryValidateObject(userData, new ValidationContext(userData),validationResults);
            
            Assert.IsTrue(modelValidation);
            Assert.NotNull(actual);
            Assert.IsInstanceOf<RedirectToActionResult>(actual);
            mockCookieMgr.Verify(y=>y.Append(It.IsAny<string>(),It.IsAny<string>(),It.IsAny<CookieOptions>(),It.IsAny<HttpResponse>()), Times.Exactly(2));

        }

    }
    
}

