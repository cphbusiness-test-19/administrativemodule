using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace module
{
    public class LoginController : Controller
    {
        private IAuthService _authservice;
        private ICookieManager _cookieManager; 
        public LoginController(IAuthService authservice, ICookieManager cookieManager)
        {
            _authservice=authservice;
            _cookieManager = cookieManager;
            _cookieManager.SetOptions(true,true);
        }

        [HttpGet]
        public IActionResult Login(){
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserModel userData){
            if(!ModelState.IsValid){
                return View(userData);
            }
            userData = await _authservice.VerifyCredentials(userData);
            
            if(userData==null){
                ViewData["message"]="Credentials incorrect";
                return View();
            }else{
                //place token in cookie 
                _cookieManager.Append("Token", userData.Token, _cookieManager.GetOptions(), Response);
                _cookieManager.Append("logged", "inside", _cookieManager.GetOptions(), Response);
                return RedirectToAction("Landing");
            }
        }

        [HttpGet]
        public IActionResult Logout(){
            _cookieManager.Delete("Token", Response);
            _cookieManager.Append("logged", "outside", _cookieManager.GetOptions(), Response);
            return Redirect("/");
        }

        [Authorize]
        public IActionResult Landing(){
            List<LandingModel> landingvm = new List<LandingModel>();
            var featurenames = new List<string>(){"ECONOMY", "COURSES", "TEACHERS", "STUDENTS","FACILITIES"}; 

            var tmp = new LandingModel(){
                FeatureName="VOTING",
                ControllerName="Voting"
            };
            landingvm.Add(tmp);

            for (int i = 0; i < 5; i++) 
            {
                var newLM = new LandingModel(){
                    FeatureName = featurenames[i],
                    ControllerName = "Course"
                };
                landingvm.Add(newLM);
            }
            return View(landingvm);
        }
    }
}