using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using NUnit.Framework;

namespace Tests
{
    [Category("integration-test")]
    [Category("ControllerIntegratiomTest")]
    [Category("fast")]
    public class ControllerIntegrationTest
    {
        //factory to create a testserver-clients
        private WebApplicationFactory<module.Startup> _factory;

        [SetUp]
        public void setup(){
            _factory = new WebApplicationFactory<module.Startup>();
        }

        [Test]
        public void smalltest(){
            Assert.Pass();
        }

        private static object[] OverkillStructPublic = {
            new object[]{"/"},
            new object[]{"/login/login"},
            new object[]{"/home"}
        };

        [Test]
        [TestCaseSource("OverkillStructPublic")]
        public async Task Public_endpoint_VerifyStatusCode(string url){
            var client = _factory.CreateClient();
        
            var response = await client.GetAsync(url);
            
            //method provided my Microsoft to verify response-statuscodes 
            response.EnsureSuccessStatusCode();

            Assert.That(response.Content.Headers.ContentType.ToString(), Is.EqualTo("text/html; charset=utf-8"));
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        private static object[] AuthorizeEndpoints ={
            new object[]{"/home/privacy"},
            new object[]{"/login/landing"},
            new object[]{"/course/index"},
            new object[]{"/course/createcourse"},
            new object[]{"/course/allcourses"},
            new object[]{"/course/editcourse"}
        };

        [Test]
        [TestCaseSource("AuthorizeEndpoints")]
        public async Task Private_endpoint_verifyUnauthorized(string url){

            var client = _factory.CreateClient(
                new WebApplicationFactoryClientOptions{
                    AllowAutoRedirect = false,
                }
            );
            var response = await client.GetAsync(url);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

    }
}