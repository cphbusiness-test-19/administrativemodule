using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace module
{
        public class CourseModel{
        public string Id { get; set; }
        [Required]
        public string CourseName { get; set; }
        public string Room { get; set; }
        public int TeacherId { get; set; }
        public List<int> Students { get; set; } = new List<int>();
        public string Category { get; set; }
        public int NoHoursPrWeek { get; set; }
        public string Weekdays { get; set; }
        public string Description { get; set; }
        public string Semester { get; set; }
        public string Language { get; set; }
        public string Level { get; set; }
        public bool Visible { get; set; } = false;
        public bool Archived { get; set; } = false;
        public int Votes { get; set; } = 0;
    }

}