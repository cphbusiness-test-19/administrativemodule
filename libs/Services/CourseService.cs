using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace module
{
    public class CourseService : ICourseService
    {
        private IHttpClientFactory _clientFactory;
        List<CourseModel> modelList;
        public CourseService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;   
            modelList = new List<CourseModel>();
        }

        public CourseModel AdjustVals(CourseModel courseData){
            if (courseData.Room==null){courseData.Room="";}
            if (courseData.Students==null){courseData.Students= new List<int>(){};}
            if (courseData.Description==null){courseData.Description="";}
            if (courseData.Semester==null){courseData.Semester="";}
            return courseData;
        }
        public async Task<bool> CreateCourse(CourseModel courseData)
        {
            var res = false;
            try
            {
                //null values not allowed in json
                var senderData = ExtractSenderData(AdjustVals(courseData));
                var client = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Post, "http://dataservice:80/courses");
                msgreq.Content = new StringContent(JsonConvert.SerializeObject(senderData),Encoding.UTF8, "application/json");

                var responseMsg= await client.SendAsync(msgreq);
                var ServiceResponse = await responseMsg.Content.ReadAsAsync<dynamic>();
                res = true;
            }
            catch (System.Exception err)
            {
                System.Console.WriteLine(err);
                res = false;
                
            }
            return res;
        }
        public async Task<bool> DeleteCourse(string id){
            var res = false;
            try
            {
                var client = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Delete, "http://dataservice:80/courses/" + id);
                var ServiceResponse = await client.SendAsync(msgreq);
                res=true;
            }
            catch (System.Exception err)
            {
                System.Console.WriteLine(err);
                res=false;
            }
            return res;
        }

        public async Task<List<CourseModel>> GetAllCoursesAsync(int maximumstudents)
        {   
            try
            {
                var client = _clientFactory.CreateClient();
                var URL = "";
                if(maximumstudents == 0 ){ URL = "http://dataservice:80/courses/";}
                else {URL = "http://dataservice:80/courses?maximumstudents=" + maximumstudents;}
                var msgreq = new HttpRequestMessage(HttpMethod.Get, URL);
                var responseMsg= await client.SendAsync(msgreq);
                var ServiceResponse = await responseMsg.Content.ReadAsAsync<List<CourseModel>>();
                modelList.AddRange(ServiceResponse);
            }
            catch (System.Exception err)
            {
               System.Console.WriteLine(err); 
            }

            return modelList;
        }

        public async Task<CourseModel> GetCourseAsync(string id)
        {
            CourseModel res= new CourseModel();
            try
            {
                var client = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Get, "http://dataservice:80/courses/"+id);
                var responseMsg= await client.SendAsync(msgreq);
                var ServiceResponse = await responseMsg.Content.ReadAsAsync<CourseModel>();
                res = ServiceResponse;
            }
            catch (System.Exception err)
            {
                System.Console.WriteLine(err);
                
            }
            return res;
        }

        public async Task<bool> UpdateCourse(CourseModel courseData)
        {
            var res = false;
            try
            {
                //fake the studentIdlist
                courseData.Students=new List<int>(){1,2,3,4,5,6,7,8,9};
                var client = _clientFactory.CreateClient();
                var senderData = ExtractSenderDataId(AdjustVals(courseData));
                var msgreq = new HttpRequestMessage(HttpMethod.Patch, "http://dataservice:80/courses/" + courseData.Id);
                msgreq.Content = new StringContent(JsonConvert.SerializeObject(senderData), Encoding.UTF8, "application/json");
                var responseMsg = await client.SendAsync(msgreq);
                var ServiceResponse = await responseMsg.Content.ReadAsAsync<dynamic>();
                res=true;
            }
            catch (System.Exception err)
            {
                System.Console.WriteLine(err);
                res=false;
            }
            return res;
        }

        public async Task<int> BulkDeleteCourses(){

            int numberDeleted = 0;
            try
            {
                var client = _clientFactory.CreateClient();
                var msgreq = new HttpRequestMessage(HttpMethod.Delete, "http://dataservice:80/courses/clean_archive");
                var responseMsg = await client.SendAsync(msgreq);
                var ServiceResponse = await responseMsg.Content.ReadAsAsync<dynamic>();

                numberDeleted = ServiceResponse["nodel"];
            }
            catch (System.Exception err)
            {
                System.Console.WriteLine(err);
            }
            return numberDeleted;
        }

        public smallCourseModel ExtractSenderData(CourseModel CMData){
            var newObj = new smallCourseModel();
            PropertyInfo[] DestFields = typeof(smallCourseModel).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            PropertyInfo[] SrcFields = typeof(CourseModel).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var item in SrcFields)
            {
                var destfield = DestFields.FirstOrDefault(x=>x.Name == item.Name);
                if (item.Name!="Votes" && item.Name!="Id"){
                    destfield.SetValue(newObj, item.GetValue(CMData));
                }
            }
            return newObj;
        }

        public UpdateCourseModel ExtractSenderDataId(CourseModel CMData){
            var newObj = new UpdateCourseModel();
            PropertyInfo[] DestFields = typeof(UpdateCourseModel).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            PropertyInfo[] SrcFields = typeof(CourseModel).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var item in SrcFields)
            {
                System.Console.WriteLine("NAME --- " + item.Name);
                var destfield = DestFields.FirstOrDefault(x=>x.Name == item.Name);
                if (item.Name!="Votes"){
                    destfield.SetValue(newObj, item.GetValue(CMData));
                }
            }
            return newObj;
        }

    }
}