using System.Collections.Generic;
using System.Threading.Tasks;

namespace module
{
    public interface ICourseService
    {
        Task<CourseModel> GetCourseAsync(string id);
        Task<List<CourseModel>> GetAllCoursesAsync(int number);
        Task<bool> CreateCourse(CourseModel courseData);
        Task<bool> DeleteCourse(string id);
        Task<int> BulkDeleteCourses();
        Task<bool> UpdateCourse(CourseModel courseData);

    }
}